package kz.astana.viewapplication;

import android.os.Bundle;
import android.util.Log;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

public class TimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        TimePicker tp = findViewById(R.id.timePicker);
        tp.setIs24HourView(true);
        tp.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                Log.d("Hello", "Time: " + hourOfDay + ":" + minute);
            }
        });
    }
}