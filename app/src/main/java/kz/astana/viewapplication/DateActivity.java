package kz.astana.viewapplication;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import java.util.Calendar;

import androidx.appcompat.app.AppCompatActivity;

public class DateActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        Calendar minDate = Calendar.getInstance();
        minDate.set(2020, 9, 10);
        Calendar maxDate = Calendar.getInstance();
        maxDate.set(2020, 10, 10);

        DatePicker dp = findViewById(R.id.datePicker);
        dp.setMinDate(minDate.getTimeInMillis());
        dp.setMaxDate(maxDate.getTimeInMillis());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            dp.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Log.d("Hello", "Date: " + dayOfMonth + "." + monthOfYear + "." + year);
                }
            });
        }
    }
}